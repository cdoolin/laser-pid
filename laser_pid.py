#
#  PID controller to adjust the wavelength of a newfocus laser
#  in order to keep the voltage measured with a nidaqmx adc constant.
#
#  Used to lock the laser to the slope of an optical resonance.
#
#  Callum Doolin (doolin@ualberta.ca), 2015
#

from PyDAQmx import *
import numpy

# use argparse module to read commandline options
import argparse

pars = argparse.ArgumentParser(description="""
wavelength pid
""")

pars.add_argument("-c", "--channel", type=str, help="nidaqmx channel to read from")
pars.add_argument("--maxv", type=float, default=2., help="range of nidaq voltage input (default 2)")
pars.add_argument("--samplerate", type=float, default=1000., help="nidaq sample rate (default 1 ksps)")
pars.add_argument("--pidsamples", type=int, default=100., help="num samples to average and give to pid (default 100)")

pars.add_argument("--lasernet", type=str, default="localhost", help="address of lasernet server (default localhost)")

pars.add_argument("-n", "--negative", action='store_true', default=False,
    help="transmission response inversly proportional to positive wavelength shift (default positive)")

pars.add_argument("--kp", type=float, default=.5, help="proportional gain")
pars.add_argument("--ki", type=float, default=.5, help="integrative gain")
pars.add_argument("--kd", type=float, default=0., help="derivitive gain")

pars.add_argument("--k", "-k", type=float, default=0., help="gain")
pars.add_argument("--ti", type=float, default=10., help="integrative time scale")

pars.add_argument("--target", "-t", type=float, default=None, help="volt set point (default current volt)")

args = pars.parse_args()

#
# configure PID controller parameters from arguments.
# can either define each gain, or one gain with time constant things.
#

if args.k is not 0.:
    kp = args.k
    ki = args.k / args.ti
    kd = 0.
else:
    kp = args.kp
    ki = args.ki
    kd = args.kd
print("kp: %.2f, ki: %.2f, kd: %.2f" % (kp, ki, kd))

pidrate = args.samplerate / args.pidsamples
print("pid at %.1f Hz" % pidrate)

#
# define PID controller class
#

class PIDController(object):
    def __init__(self, kp=.5, ki=0., kd=0., dT=1.):
        self.kp = kp
        self.ki = ki
        self.kd = kd

        self.dT = dT

        self.error_sum = 0
        self.prev_error = 0

    def step(self, error):
        # compute ki in integral incase it's time dependant
        self.error_sum += self.ki * error * self.dT
        dedt = (error - self.prev_error) / self.dT

        u = self.kp * error + self.error_sum + self.kd * dedt
        return u

#
# configure nidaqmx
#

# from PyDAQmx import *
# import numpy

task = Task()
read = int32()
if args.pidsamples > 1024 / 2:
    print("warning!  pid samples greater than half the buffer size. things will probably break.")

buff = numpy.zeros((1024,), dtype=numpy.float64)

task.CreateAIVoltageChan(args.channel, "",  # no custom name
                         DAQmx_Val_RSE,        # measure w/ respect to ground
                        -args.maxv, args.maxv, # v lims
                        DAQmx_Val_Volts, None) # volts, not custom scale

task.CfgSampClkTiming("", args.samplerate, DAQmx_Val_Rising, # config onboard clock
                      DAQmx_Val_ContSamps, 1024)  # continuos & buff size

# create function for easy voltage getting

def get_volt():
    task.ReadAnalogF64(int(args.pidsamples), 10.0, # nsamps, timeout
                    DAQmx_Val_GroupByChannel, buff, len(buff),
                    byref(read), None)
    return numpy.mean(buff[:read.value])

# collect current voltage
if args.target is None:
    task.StartTask()
    volt_target = get_volt()
    task.StopTask()
else:
    volt_target = args.target

#
# configure lasernet client for wavelength feedback
#

from labdrivers import websocks
laser = websocks.LaserClient(server=args.lasernet)

piezo = laser.get_volt();

#
# create pid controller
#

pid = PIDController(kp=kp, ki=ki, kd=kd, dT=pidrate)

# set error_sum to be the current wavelength, so when the pid controller runs
# it outputs the current wavelength since error should be 0.
pid.error_sum = piezo

#
# start control loop
#

direction = -1 if args.negative else 1

task.StartTask()
i = 0
while True:
    volt = get_volt()
    error = direction*(volt - volt_target)
    piezo = pid.step(error)

    if piezo > 100. or piezo < 0:
        print("piezo out of range. exiting...")
        break

    laser.set_volt(piezo)

    if i % 4 is 0:
        print("%.3f V, %.3f %%" % (volt, piezo))
    i = i + 1

task.StopTask()
