# laser_pid

Implements a simple PID to controller the wavelength of a tunable NewFocus laser.

Measures a voltage on a NI DAQ card, and controls the wavelength with [labdrivers](https://github.com/cdoolin/labdrivers)